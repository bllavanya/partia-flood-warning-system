# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from typing import List, Tuple, Set, Dict
from collections import defaultdict
from haversine import haversine, Unit

from .station import MonitoringStation
from .stationdata import build_station_list
from .utils import sorted_by_key  # noqa


def stations_by_distance(
    stations: List[MonitoringStation], p: Tuple[float, float]
) -> List[Tuple[MonitoringStation, float]]:
    station_by_distance = [
        (station, haversine(station.coord, p, unit=Unit.KILOMETERS))
        for station in stations
    ]
    return sorted_by_key(station_by_distance, 1)


def stations_within_radius(
    stations: List[MonitoringStation], centre: Tuple[float, float], r: int
) -> List[str]:
    by_distance: List[Tuple[MonitoringStation, float]] = stations_by_distance(
        stations, centre
    )
    station_names: List[str] = [
        station.name for station, distance in by_distance if distance <= r
    ]
    return sorted(station_names)


def rivers_with_station(stations: List[MonitoringStation]) -> Set[str]:
    return {station.river for station in stations}


def stations_by_river(
    stations: List[MonitoringStation],
) -> Dict[str, List[MonitoringStation]]:
    by_river: Dict[str, List[MonitoringStation]] = defaultdict(list)
    for station in stations:
        by_river[station.river].append(station)
    return by_river


def rivers_by_station_number(
    stations: List[MonitoringStation], N: int
) -> List[Tuple(str, int)]:
    by_river = stations_by_river(stations)
    river_station_count = {k: len(v) for k, v in by_river.items()}
    river_station_count = sorted_by_key(river_station_count, 1)
