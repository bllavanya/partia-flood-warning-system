from typing import List, Tuple
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius, stations_by_river


def print_stations_within_10km_radius() -> None:
    stations: List[MonitoringStation] = build_station_list()
    centre: Tuple[float, float] = (52.2053, 0.1218)
    radius_in_kms: int = 10
    list_of_stations: List[str] = stations_within_radius(
        stations, centre, radius_in_kms
    )
    print(
        f"Stations with {radius_in_kms} KMs from the centre {centre} are: {list_of_stations}"
    )


def print_stations_by_river(river_name: str) -> None:
    stations: List[MonitoringStation] = build_station_list()
    by_river = sorted(
        [station.name for station in stations if station.river == river_name]
    )
    print(by_river)


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    print_stations_within_10km_radius()
    print_stations_by_river("River Aire")
    print_stations_by_river("River Cam")
    print_stations_by_river("River Thames")
